# barsgroup

Тестовое задание для БАРС.ГРУП


# Инструкция
1. Установить/Убедиться что установлен php PHP 7.1 и выше
2. Установить/Убедиться что установлен php-fpm
3. Убедиться что установлен nginx версии 1.12 и выше
```
nginx version: nginx/1.14.0 (Ubuntu)
```
3.1 Настроить конфигурацию для сайта.

В строке 
>set $root /www/barsgroup

вмecто www написать путь до webroot
```
server {
    server_name     bars.ru;
    listen 80;

    resolver                8.8.8.8 valid=3600s;
    resolver_timeout        120s;

    set $root       /www/barsgroup;
    root    $root;
    index   index.php;
    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
	
        # With php-fpm (or other unix sockets):
        fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
        # With php-cgi (or other tcp sockets):
        fastcgi_pass 127.0.0.1:9000;
    }

}
```
5. Загрузить проект в webroot
```
cd %webroot
git clone git@gitlab.com:Kamstan/barsgroup.git
```
6. Открыть сайт по адресу указаному в конфиге файле сайта.

>server_name     bars.ru;