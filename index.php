<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

function __autoload($class_name)
{
    include "src/$class_name.php";
}

$app = new App();
$app->run();