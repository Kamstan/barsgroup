<?php

class App
{
    /** @var Collection $lpu_list */
    protected $lpu_list;
    protected $path;

    public function __construct()
    {
        $this->path = __DIR__ . '/lpu.json';
        if (!file_exists($this->path))
            throw new Exception("DB file not found");

        $lpu_list = json_decode(file_get_contents($this->path), true);
        $this->lpu_list = new Collection($lpu_list['LPU']);
    }

    public function run()
    {
        $uri = trim($_SERVER['REQUEST_URI'],'/');
        if (!empty($uri)) {
            $uri = trim($_SERVER['REQUEST_URI'], '/');
            $actionName = "action".ucfirst($uri);
            $this->$actionName();
        } else {
            $this->actionIndex();
        }
    }

    protected function actionIndex()
    {
        echo $this->render('view/index.php', [
            'collection' => $this->lpu_list
        ]);
    }

    protected function actionChildren()
    {
        $id = (int)$_POST['id'];
        echo $this->render('view/_children.php', [
            'children' => $this->lpu_list->searchChild($id)
        ]);
    }

    protected function actionEdit()
    {
        $lpu = $_POST['Lpu'];
        $key = $this->lpu_list->searchById($lpu['id']);
        $this->lpu_list->offsetSet($key, $lpu);
        $this->saveFile();
    }

    protected function actionDelete()
    {
        $id = (int)$_POST['id'];
        $key = $this->lpu_list->searchById($id);
        $this->lpu_list->offsetUnset($key);
        $this->saveFile();
    }

    protected function actionAdd()
    {
        $post = $_POST['Lpu'];
        $post['id'] = $this->lpu_list->getLastId() + 1;
        $this->lpu_list->append($post);
        $this->saveFile();
        echo $post['id'];
}

    private function saveFile()
    {
        $array = $this->lpu_list->getArrayCopy();
        $json = json_encode(['LPU' => array_values($array)], JSON_UNESCAPED_UNICODE );
        file_put_contents(__DIR__.'/lpu.json', $json);
    }

    protected function render($view, array $data = [])
    {
        ob_start();
        ob_implicit_flush(false);
        extract($data, EXTR_OVERWRITE);
        require($view);

        return ob_get_clean();
    }
}