<?php

class Collection extends ArrayIterator
{
    /** @var []Lpu $_cache */
    protected $_cache;

    public function current()
    {
        if (empty($this->_cache[parent::key()])) {
            $this->_cache[parent::key()] = new Lpu(parent::current());
        }
        return $this->_cache[parent::key()];
    }

    public function offsetGet($key)
    {
        if (empty($this->_cache[$key])) {
            $this->_cache[$key] = new Lpu(parent::offsetGet($key));
        }
        return $this->_cache[$key];
    }

    public function offsetUnset($key)
    {
        parent::offsetUnset($key);
        unset($this->_cache[$key]);
    }

    public function searchById(int $id)
    {
        $this->rewind();
        while ($this->valid()) {
            $lpu = $this->current();
            if ($id == $lpu->id) {
                return $this->key();
            }
            $this->next();
        }
        return null;
    }

    public function searchChild($parent_id)
    {
        $child = array();
        $this->rewind();
        while ($this->valid()) {
            $lpu = $this->current();
            if ($parent_id == $lpu->hid) {
                $child[$this->key()] = $lpu;
            }
            $this->next();
        }
        return $child;
    }

    public function getLastId()
    {
        $maxId = 0;
        $this->rewind();
        while ($this->valid()) {
            $lpu = $this->current();
            if ($maxId < $lpu->id) {
                $maxId = $lpu->id;
            }
            $this->next();
        }
        return $maxId;
    }
}