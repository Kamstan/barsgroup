<?php

class Lpu
{
    public $id = null, $hid = null, $full_name = null, $address = null, $phone = null;

    public function __construct(array $lpu = [])
    {
        $this->setAttr($lpu);
    }

    public function setAttr(array $attr = [])
    {
        foreach ($attr as $key => $item) {
            $this->$key = $item;
        }
    }
}