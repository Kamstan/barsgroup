function qEdit() {
    var name, oldName, nameText;
    var address, oldAddress, addressText;
    var phone, oldPhone, phoneText;
    var btnBlock, parent;

    this.edit = function (e) {
        parent = $(e.target).closest('tr');
        btnBlock = $('.buttons', parent);
        name = $('.lpu__name', parent);
        oldName = name.html();
        nameText = $('span', name).text().replace(/"/g, '&quot;').trim();
        address = $('.lpu__address', parent);
        oldAddress = address.html();
        addressText = $('span', address).text().replace(/"/g, '&quot;').trim();
        phone = $('.lpu__phone', parent);
        oldPhone = phone.html();
        phoneText = $('span', phone).text().replace(/"/g, '&quot;').trim();
        name.html('<input type="text" class="form-control" name="Lpu[name]" value="'+nameText+'" data-text="'+nameText+'" />');
        address.html('<input type="text" class="form-control" name="Lpu[address]" value="'+addressText+'" data-text="'+addressText+'" />');
        phone.html('<input type="text" class="form-control" name="Lpu[phone]" value="'+phoneText+'" data-text="'+phoneText+'" />');
        btnBlock.html('<span class="hidden">'+btnBlock.html()+'</span>' +
            '<a class="btn btn-sm btn-success saveEdit" title="Сохранить"><i class="glyphicon glyphicon-ok"></i></a>&nbsp;'+
            '<a class="btn btn-sm btn-danger cancelEdit" title="Отмена"><i class="glyphicon glyphicon-ban-circle"></i></a>'
        );
        return false;
    };
    this.cancel = function(e) {
        name.html(oldName);
        address.html(oldAddress);
        phone.html(oldPhone);
        btnBlock.html($('span.hidden',btnBlock).html());
        return false;
    };
    this.save = function(e) {
        $.ajax({
            url: $('.edit',parent).attr('href'),
            type: 'POST',
            data: {
                'Lpu[id]': parent.attr('id'),
                'Lpu[hid]': parent.attr('data-hid'),
                'Lpu[full_name]': $('input',name).val(),
                'Lpu[address]': $('input',address).val(),
                'Lpu[phone]': $('input',phone).val()
            },
            success: function(data){
                name.html('<span>'+$('input',name).val()+'</span>');
                address.html('<span>'+$('input',address).val()+'</span>');
                phone.html('<span>'+$('input',phone).val()+'</span>');
                btnBlock.html($('span.hidden',btnBlock).html());
            },
            error: function(request){
                alert(request.responseText);
            }
        });
        return false;
    }
}
var edit = new qEdit();
var lpu = $('.lpu,.lpu_child');
lpu.on('click', '.edit', edit.edit);
lpu.on('click', '.cancelEdit', edit.cancel);
lpu.on('click', '.saveEdit', edit.save);
