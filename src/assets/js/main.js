
function toggle(elm) {
    var row = elm.parentNode.parentNode;
    if (!row.dataset.upload) {
        $.post('/children',{'id':row.id}, function (data) {
            $(row).after(data);
            row.dataset['upload'] = true;
        });
        $('b',elm).html('-');
    } else {
        delChild(row);
        delete row.dataset['upload'];
        $('b',elm).html('+');
    }
}

function delChild(parent) {
    var next = parent.nextElementSibling;
    if ($(next).hasClass('lpu_child')) {
        $(next).remove();
        delChild(parent);
    }
}

function allToggle() {
    $.each($('.lpu'), function (index) {
        $(this).find('.toggle').trigger('click');
    })
}

$(document).ready(function(){
    var lpu = $('.lpu');
    lpu.on('click', '.remove', remove);
    lpu.on('click', '.add', add);
    $('.add-new').on('click', add);
});

function add() {
    var parent = $(this).closest('tr');
    parent.after('<tr class="row"><td class="col-1"></td>'+
        '<td class="lpu__name col-3">'+buildInput('name')+'</td>'+
        '<td class="lpu__address col-4">'+buildInput('address')+'</td>'+
        '<td class="lpu__phone col-2">'+buildInput('phone')+'</td>'+
        '<td class="buttons col-2">'+
        '<a class="btn btn-sm btn-success saveAdd" title="Сохранить"><i class="glyphicon glyphicon-ok"></i></a>&nbsp;'+
        '<a class="btn btn-sm btn-danger cancelAdd" title="Отмена"><i class="glyphicon glyphicon-ban-circle"></i></a>'+
        '</td></tr>');
    $('.saveAdd').on('click', saveAdd);
    $('.cancelAdd').on('click', cancelAdd);
    return false;
}

function saveAdd() {
    var parent = $(this).closest('tr');
    var name = $('.name', parent);
    var address = $('.address', parent);
    var phone = $('.phone', parent);
    var blockBtn = $(this).parent();
    $.ajax({
        url: $('.add',parent.previousSibling).attr('href'),
        type: 'POST',
        data: {
            'Lpu[hid]': parent.prev().attr('id'),
            'Lpu[full_name]': name.val(),
            'Lpu[address]': address.val(),
            'Lpu[phone]': phone.val()
        },
        success: function(data){
            console.log(data);
            name.parent().html('<span>'+name.val()+'</span>');
            address.parent().html('<span>'+address.val()+'</span>');
            phone.parent().html('<span>'+phone.val()+'</span>');
            parent.attr('id', data);
            if (parent.prev().hasClass('lpu')) {
                parent.prev().attr('data-upload','true');
                parent.attr('data-hid', parent.prev().attr('id'));
                $('b',parent.prev()).html('-');
                parent.addClass('lpu_child');
                blockBtn.html(' <a class="btn btn-sm btn-primary edit" href="/edit">Редактировать</a>'+
                    '<a class="btn btn-sm btn-danger remove" href="/delete">Удалить</a>');
            } else {
                parent.addClass('lpu');
                parent.find('td:first').html('<a onclick="toggle(this)" class="toggle"><b>+</b></a>');
                blockBtn.html(' <a class="btn btn-sm btn-primary edit" href="/edit">Редактировать</a>'+
                    '<a class="btn btn-sm btn-success add" href="/add">Добавить подразделение</a>'+
                    '<a class="btn btn-sm btn-danger remove" href="/delete">Удалить</a>');
            }

            var edit = new qEdit();
            var lpu = $('.lpu,.lpu_child');
            lpu.on('click', '.edit', edit.edit);
            lpu.on('click', '.cancelEdit', edit.cancel);
            lpu.on('click', '.saveEdit', edit.save);
            lpu.on('click', '.remove', remove);
            lpu.on('click', '.add', add);
        },
        error: function(request){
            alert(request.responseText);
        }
    });
}

function cancelAdd() {
    var parent = $(this).closest('tr');
    parent.remove();
}

function buildInput(name) {
    return '<input type="text" class="form-control '+name+'" name="Lpu['+name+']" />';
}

function remove() {
    var row = this.parentNode.parentNode;
    $.post('/delete',{'id':row.id}, function () {
        $(row).remove();
    });
    return false;
}