<?php
/** @var array $children */
?>
<?php foreach ($children as $child): ?>
<tr id="<?= $child->id ?>" data-hid="<?= $child->hid ?>" class="lpu_child row">
    <td></td>
    <td class="lpu__name col-3"><span><?= $child->full_name ?></span></td>
    <td class="lpu__address col-4"><span><?= $child->address ?></span></td>
    <td class="lpu__phone col-2"><span><?= $child->phone ?></span></td>
    <td class="buttons col-2">
        <a class="btn btn-sm btn-primary edit" href="/edit">Редактировать</a>
        <a class="btn btn-sm btn-danger remove" href="/delete">Удалить</a>
    </td>
</tr>
<?php endforeach; ?>
<script>
    $('.lpu_child').on('click', '.remove', remove);
</script>
<script src="/src/assets/js/editor.js"></script>
