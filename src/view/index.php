<?php
/** @var ArrayIterator $collection */
?>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="/src/assets/сss/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="/src/assets/js/main.js"></script>
</head>
<body>
    <div class="container">
        <table>
            <thead>
                <tr>
                    <th></th>
                    <th>Наименование</th>
                    <th>Адрес</th>
                    <th>Телефон</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <tr class="row">
                <td id="all-toggle" onclick="allToggle(this)">Свернуть все/Развернуть все</td>
                <td></td><td></td><td></td>
                <td class="buttons"><a class="btn btn-sm btn-success add-new" href="/add">Добавить</a></td>
            </tr>
                <?php while ($collection->valid()): ?>
                    <?php $lpu = $collection->current(); ?>
                    <?php if (empty($lpu->hid)): ?>
                        <tr id="<?= $lpu->id ?>" class="lpu row">
                            <td class="col-1">
                                <a onclick="toggle(this)" class="toggle"><b>+</b></a>
                            </td>
                            <td class="lpu__name col-3"><span><?= $lpu->full_name ?></span></td>
                            <td class="lpu__address col-4"><span><?= $lpu->address ?></span></td>
                            <td class="lpu__phone col-2"><span><?= $lpu->phone ?></span></td>
                            <td class="buttons col-2">
                                <a class="btn btn-sm btn-primary edit" href="/edit">Редактировать</a>
                                <a class="btn btn-sm btn-success add" href="/add">Добавить подразделение</a>
                                <a class="btn btn-sm btn-danger remove" href="/delete">Удалить</a>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php $collection->next(); ?>
                <?php endwhile; ?>
            </tbody>
        </table>
    </div>
</body>

<script src="/src/assets/js/editor.js"></script>
